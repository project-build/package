# package

## 介绍
### 项目打包实战持续更新：
- 1.整个项目打war包 (maven构建或者gradle构建或者ant构建)  
- 2.整个项目单独jar (maven构建或者gradle构建或者ant构建)  
- 3.整个项目配置，资源，依赖lib分开且只将类文件单独打jar  (maven构建或者gradle构建或者ant构建) 
- 4.整个项目打成单独jar同时通过dockerfile构建镜像 (maven结合docker构建或者gradle结合docker构建或者ant结合docker构建) 
- 5.整个项目配置，资源，依赖lib分开且只将类文件单独打jar同时通过dockerfile构建镜像 (maven结合docker构建或者gradle结合docker构建或者ant结合docker构建) 

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
