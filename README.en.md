# package

#### Description
项目打包实战持续更新：
1.整个项目打war包 (maven构建或者gradle构建或者ant构建)  
2.整个项目单独jar (maven构建或者gradle构建或者ant构建)  
3.整个项目配置，资源，依赖lib分开且只将类文件单独打jar  (maven构建或者gradle构建或者ant构建) 
4.整个项目打成单独jar同时通过dockerfile构建镜像 (maven结合docker构建或者gradle结合docker构建或者ant结合docker构建) 
5.整个项目配置，资源，依赖lib分开且只将类文件单独打jar同时通过dockerfile构建镜像 (maven结合docker构建或者gradle结合docker构建或者ant结合docker构建) 

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
